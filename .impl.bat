@echo off
setlocal EnableDelayedExpansion
set versionScript=v9.2.0
set versionDate=2025-01-25
echo [93mcreate%1Api %versionScript% / %versionDate%[0m

if "%2"=="" (
  echo [95musage:[0m create%1Api type solutionName targetFolder [pathToDbFile] [tableName]
  echo [96mtype[0m:         Type of project ^(Web^|Min^|All^)
  echo [96msolutionName[0m: Name of the solution and name of folder
  echo [96mtargetFolder[0m: Folder where to generate the project to.
  echo [96mpathToDbFile[0m: If set, an Entity data model will be generated. DB has to reside in local folder or absolute path given.
  echo [96mtableName[0m:    For this table a Get-Endpoint is generated. Default: Categories
  exit/b
)
set currentPath=%~dp0
set type=%1
set solutionName=%2
set projectName=%2
set targetFolder=%3
set dbFile=%4
set tableName=%5
set isAbsoluteDbPath=false
set httpPort=5000
set httpsPort=7001

set isTypeOk=false
if "%type%"=="Web" set isTypeOk=true
if "%type%"=="Min" set isTypeOk=true
if "%type%"=="All" set isTypeOk=true
if %isTypeOk%==false (
    echo [95mInvalid type:[0m Type of project must be one of the following Web^|Min^|All
	exit/b
)

set useWeb=true
set useMin=true
if %type%==Min (
    set useWeb=false
)
if %type%==Web (
    set useMin=false
)

if "%targetFolder%"=="" set targetFolder=.

echo [7m----------------------------------------[0m
echo [95m.Net Core %type% Api project create script[0m
echo [95m          !versionScript!, !versionDate!         [0m
echo [95m  (C)Robert Grueneis/HTL Grieskirchen [0m
echo [7m----------------------------------------[0m

::------------------------------------------------------------ set versions
echo [95mCurrent dotnet version[0m:
dotnet --version
echo [95mCurrent dotnet-ef version[0m:
dotnet-ef --version
echo [7m---------------------------------------[0m
for /F "tokens=* USEBACKQ" %%F in (`dotnet --version`) do (
set temp=%%F
)
set netcoreVersion=%temp:~0,3%
for /F "tokens=* USEBACKQ" %%F in (`dotnet-ef --version`) do (
set efVersion=%%F
)
set netcoreVersionMajor=%netcoreVersion:~0,1%
set efVersionMajor=%efVersion:~0,1%
echo Using .Net Core:        [96m%netcoreVersion%[0m
echo Using Entity Framework: [96m%efVersion%[0m
if "%netcoreVersionMajor%" LSS "8" (
  echo [91mwrong version of DotNet Core[0m - at least version 8 required
  exit/b
)
if "%efVersionMajor%" LSS "8" (
  echo [91mmissing or wrong version of Entity Framework Core[0m - at least version 8 required
  if "%efVersion%"=="" set efVersion=8
  echo if not available install with: [92mdotnet tool install --global dotnet-ef --version 8[0m
  echo if is  available update  with: [92mdotnet tool update  --global dotnet-ef --version 8[0m
  echo perhaps you have to add the path of your user-home /.dotnet/tools
  exit/b
)
::echo [95mAll dotnet versions[0m:
::dotnet --list-sdks
::echo [7m-----------------------[0m
echo [7m---------------------------------------[0m

::------------------------------------------------------------ set variables
if "%solutionName%"=="" (set solutionName=My%type%Api)
if "%projectName%"=="" (set projectName=%solutionName%)

set withDb=false
if not "%dbFile%"=="" set withDb=true

if %withDb%==false goto skipDb
  set fullMdfPath=%currentPath%\%dbFile%
  if not x%dbFile::=%==x%dbFile% (
    set fullMdfPath=%dbFile%
	set isAbsoluteDbPath=true
  )
  if not exist "!fullMdfPath!" (
	echo [91mDb-File %fullMdfPath% does not exist![0m
	exit/b
  )
  if "%tableName%"=="" set tableName=Categories

set isMdf=true
if "%dbFile:~-3%"=="ite" set isMdf=false
if "%dbFile:~-3%"==".db" set isMdf=false

%currentPath%\ExtractDbName %fullMdfPath%
set /p dbName=<tempDbName.txt
if %dbName%==Northwnd set dbName=Northwind
del /f tempDbName.txt
::exit /b

:skipDb
set url=http://localhost:%httpPort%
set urlssl=https://localhost:%httpsPort%

::------------------------------------------------------------ log setup
echo [95msolution[0m      --^> [96m%solutionName%[0m
echo [95mproject[0m       --^> [96m%projectName%[0m
if %withDb%==true (echo [95mdatabase[0m      --^> [96m%dbFile%[0m) else (echo without creating EFCore)
if not "%targetFolder%"=="" echo [95mtarget folder[0m --^> [96m%targetFolder%\%solutionName%[0m
echo [7m---------------------------------------[0m

if %withDb%==true (echo Database name = %dbName%)
if %withDb%==true (echo fullDbPath = %fullMdfPath%)

if "%targetFolder%"=="" (set targetFolder=".")
cd /d %targetFolder%

::------------------------------------------------------------ create solution and projects
mkdir %solutionName%
cd %solutionName%
set projectRoot="%cd%"
echo [92mCreating new solution[0m [96m%solutionName%[0m to [96m%projectRoot%[0m
::exit /b
dotnet new sln

::------------------------------------------------------------ create WebApi project and add to solution
mkdir %projectName%
cd %projectName%
echo [92mCreating new %type%Api Project[0m [96m%projectName%[0m
dotnet new web
cd..
echo [92mAdding project to solution[0m
dotnet sln %solutionName%.sln add %projectName%

cd %projectName%
if %useWeb%==false goto generalPackages
echo [92mAdding package for Microsoft.VisualStudio.Web.CodeGeneration.Design (version %netcoreVersion%)[0m...
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design -v %netcoreVersion% -n
echo [92mAdding package for GrueneisR.SignalRAnalyzer (lastest version)[0m...
dotnet add package GrueneisR.SignalRAnalyzer -n

:generalPackages
::------------------------------------------------------------ generalPackages
echo [92mAdding package for Swashbuckle.AspNetCore (lastest version)[0m...
dotnet add package Swashbuckle.AspNetCore -n
echo [92mAdding package for GrueneisR.RestClientGenerator (lastest version)[0m...
dotnet add package GrueneisR.RestClientGenerator -n

echo [92mCreating folder for Dtos[0m
mkdir Dtos
echo namespace %projectName%.Dtos;> Dtos\ProgramYourDtosInThisFolder.cs
echo public class DummyDtoClass { }>> Dtos\ProgramYourDtosInThisFolder.cs

echo [92mCreating folder for Services[0m
mkdir Services
echo namespace %projectName%.Services;> Services\ProgramYourServicesInThisFolder.cs
echo public class DummyServiceClass { }>> Services\ProgramYourServicesInThisFolder.cs

if %useWeb%==false goto skipControllers
echo [92mCreating folder for Controllers[0m
mkdir Controllers
if %withDb%==false echo namespace %projectName%.Controllers;> Controllers\ProgramYourControllersInThisFolder.cs
if %withDb%==false echo public class DummyControllerClass { }>> Controllers\ProgramYourControllersInThisFolder.cs

:skipControllers
::------------------------------------------------------------ skipControllers
if %useMin%==false goto packagesEnd
echo [92mCreating folder for Maps[0m
mkdir Maps
echo namespace %projectName%.Maps;> Maps\ProgramYourMapsInThisFolder.cs
echo public class DummyMapClass {}>> Maps\ProgramYourMapsInThisFolder.cs

:packagesEnd
if %withDb%==false goto adapting

::------------------------------------------------------------ create Db project and add to solution
cd..
set dbProject=%dbName%Db
mkdir %dbProject%
cd %dbProject%
echo [92mCreating new Db Class Library Project[0m [96m%dbProject%[0m
dotnet new classlib -f net%netcoreVersion%

echo [92mAdding package for EntityFrameworkCore (version %efVersion%)[0m...
dotnet add package Microsoft.EntityFrameworkCore -v %efVersion% -n
echo [92mAdding package for EntityFrameworkCore.Design (version %efVersion%)[0m...
dotnet add package Microsoft.EntityFrameworkCore.Design -v %efVersion% -n
echo [92mAdding package for EntityFrameworkCore.Tools (version %efVersion%)[0m...
dotnet add package Microsoft.EntityFrameworkCore.Tools -v %efVersion% -n

if %isMdf%==true echo [92mAdding package for EntityFrameworkCore.SqlServer (version %efVersion%)[0m...
if %isMdf%==true dotnet add package Microsoft.EntityFrameworkCore.SqlServer -v %efVersion% -n

if %isMdf%==false echo [92mAdding package for EntityFrameworkCore.Sqlite (version %efVersion%)[0m...
if %isMdf%==false dotnet add package Microsoft.EntityFrameworkCore.Sqlite -v %efVersion% -n

echo database path is absolute: [96m%isAbsoluteDbPath%[0m
set targetMdfPath=%cd%\%dbFile%
if %isAbsoluteDbPath%==true set targetMdfPath=%fullMdfPath%
if %isAbsoluteDbPath%==false echo [92mCopying Db-File[0m [96m%fullMdfPath%[0m
if %isAbsoluteDbPath%==false copy %fullMdfPath% !targetMdfPath!

echo [92mCreating Entity classes[0m for [96m%targetMdfPath%[0m
if %isMdf%==true dotnet ef dbcontext scaffold "Server=(LocalDB)\mssqllocaldb;attachdbfilename=!targetMdfPath!;integrated security=True" Microsoft.EntityFrameworkCore.SqlServer -f -c %dbName%Context --no-onconfiguring
if %isMdf%==false dotnet ef dbcontext scaffold "data source=!targetMdfPath!" Microsoft.EntityFrameworkCore.Sqlite -f -c %dbName%Context --no-onconfiguring

if %isMdf%==false echo [96m%dbName%Context.cs[0m
if %isMdf%==false %currentPath%\ReplaceInFile .\%dbName%Context.cs .\%dbName%Context.cs ValueGeneratedNever=ValueGeneratedOnAdd

set dbProjectPath=%cd%\%dbProject%.csproj
cd..
echo [92mAdding Db project to solution[0m
dotnet sln %solutionName%.sln add %dbProject%
cd %projectName%
echo [92mAdding reference to Db project[0m
dotnet add reference %dbProjectPath%


:adapting
::------------------------------------------------------------ adapting
echo [92mAdapting original files (CORS, Swagger, ...)[0m
echo [96mProgram.cs[0m
set addControllers=builder.Services.AddControllers()_semicolon_
if %useWeb%==false set addControllers=
set mapControllers=app.MapControllers()_semicolon_
if %useWeb%==false set mapControllers=
set appMaps=
set dummyDto=
set generatedRoute=dummy
if %useMin%==true set appMaps="app.MapGet(_quote_/dummy_quote_,_blank_()_blank__eq__gt__blank_new_blank_DummyDto(_quote_Quaxi_quote_,_blank_666))_semicolon_"
if %useMin%==true set dummyDto="record_blank_DummyDto(string_blank_Name,_blank_int_blank_Value)_semicolon_"
if %withDb%==false %currentPath%\ReplaceInFile %currentPath%\_templates\Program.cs .\Program.cs $$PROJECT$$=%projectName%;$$PROJECTROOT$$=%projectRoot%;$$HTTP_PORT$$=%httpPort%;$$VERSIONSCRIPT$$=%versionScript%;$$VERSIONDATE$$=%versiondate%;$$APP_MAPS$$=%appMaps%;$$DUMMY_DTO$$=%dummyDto%;$$ADD_CONTROLLERS$$=%addControllers%;$$MAP_CONTROLLERS$$=%mapControllers%

echo [96mappsettings.Development.json[0m
%currentPath%\ReplaceInFile %currentPath%\_templates\appsettings.Development.json appsettings.Development.json $$NOTHING$$=$$NITSCHEWO$$

if %useWeb%==true if %withDb%==false echo [96mValuesController.cs[0m
if %useWeb%==true if %withDb%==false %currentPath%\ReplaceInFile %currentPath%\_templates\WebApiNoDb\ValuesController.cs Controllers\ValuesController.cs $$PROJECT$$=%projectName%
if %useWeb%==true if %withDb%==false set generatedRoute=Values/Dummy
if %useWeb%==true if %withDb%==true set generatedRoute=Values/%tableName%
if %useMin%==true if %withDb%==false set generatedRoute=dummy
if %useMin%==true if %withDb%==true set generatedRoute=dbTest/%tableName%
if %withDb%==false goto finished

set url=http://localhost:%httpPort%/%generatedRoute%
set urlssl=https://localhost:%httpsPort%/%generatedRoute%

set program=Program_Mdf.cs
if %isMdf%==false set program=Program_Sqlite.cs
if %useMin%==true set appMaps="app.MapDbTests()_semicolon_"
%currentPath%\ReplaceInFile %currentPath%\_templates\!program! .\Program.cs $$PROJECT$$=%projectName%;$$DBPROJECT$$=%dbProject%;$$PROJECTROOT$$=%projectRoot%;$$DBNAME$$=%dbName%;$$DBFILE$$=%dbFile%;$$FULLDBPATH$$=%fullMdfPath%;$$HTTP_PORT$$=%httpPort%;$$VERSIONSCRIPT$$=%versionScript%;$$VERSIONDATE$$=%versiondate%;$$APP_MAPS$$=%appMaps%;$$DUMMY_DTO$$=%dummyDto%;$$ADD_CONTROLLERS$$=%addControllers%;$$MAP_CONTROLLERS$$=%mapControllers%

echo [96mappsettings.json[0m
set appsettings=appsettings_mdf.json
if %isMdf%==false set appsettings=appsettings_sqlite.json
%currentPath%\ReplaceInFile %currentPath%\_templates\!appsettings! appsettings.json $$PROJECT$$=%projectName%;$$DBNAME$$=%dbName%;$$FULLDBPATH$$=%fullMdfPath%;$$FILENAME$$_FILENAMEONLY=%fullMdfPath%;\=\\

if %useMin%==true echo [96mDbTestApi.cs[0m
if %useMin%==true %currentPath%\ReplaceInFile %currentPath%\_templates\MinApi\DbTestApi.cs Maps\DbTestApi.cs $$PROJECT$$=%projectName%;$$DBPROJECT$$=%dbProject%;$$DBNAME$$=%dbName%;$$TABLE$$=%tableName%

if %useWeb%==true echo [96mValuesController.cs[0m
if %useWeb%==true %currentPath%\ReplaceInFile %currentPath%\_templates\WebApi\ValuesController.cs Controllers\ValuesController.cs $$PROJECT$$=%projectName%;$$DBPROJECT$$=%dbProject%;$$DBNAME$$=%dbName%;$$TABLE$$=%tableName%

:finished

echo [96mlaunchSettings.json[0m
%currentPath%\ReplaceInFile %currentPath%\_templates\launchSettings.json Properties\launchSettings.json $$PROJECT$$=%projectName%;$$TABLE$$=%tableName%;$$HTTP_PORT$$=%httpPort%;$$HTTPS_PORT$$=%httpsPort%;$$MAIN_ROUTE$$=%mainRoute%;$$GENERATED_ROUTE$$=%generatedRoute%

echo [96mGlobalUsings.cs[0m
set addedUsing=
set dbUsing=
set dbProjectUsing=
if %useMin%==true set addedUsing="global using %projectName%.Maps_semicolon_"
if %withDb%==true set dbUsing="global using Microsoft.EntityFrameworkCore_semicolon_"
if %withDb%==true set dbProjectUsing="global using %dbProject%_semicolon_"
%currentPath%\ReplaceInFile %currentPath%\_templates\GlobalUsings.cs GlobalUsings.cs $$PROJECT$$=%projectName%;$$DBPROJECT_USING$$=%dbProjectUsing%;$$ADDED_USING$$=%addedUsing%;$$DB_USING$$=%dbUsing%;

echo [96mExtensionMethods.cs[0m
%currentPath%\ReplaceInFile %currentPath%\_templates\ExtensionMethods.cs ExtensionMethods.cs $$PROJECT$$=%projectName%
echo [96m_requests.http[0m
%currentPath%\ReplaceInFile %currentPath%\_templates\_requests.http _requests.http $$GENERATED_ROUTE$$=%generatedRoute%
echo [96mConsoleLoggerExtensions.cs[0m
mkdir Logging
%currentPath%\ReplaceInFile %currentPath%\_templates\ConsoleLoggerExtensions.cs Logging\ConsoleLoggerExtensions.cs $$PROJECT$$=%projectName%

echo [7m-----------------------[0m
echo [95mInstallation finished![0m
echo [7m-----------------------[0m
echo Start swagger with url [96mhttp://localhost:!httpPort![0m or [96mhttps://localhost:!httpsPort![0m...
echo Press any key to start the project and open browser with url [96m!url![0m or [96m!urlssl![0m...
pause >nul

start dotnet watch run
