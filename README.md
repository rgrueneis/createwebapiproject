# createWebApi



## Getting started

This batch script for windows is used to create a C# WebApi backend, optionally including access to a database (formats Mdf and Sqlite are supported).
Note:
* CORS is activated in the backend (without restrictions, i.e. allowing all).
* The generated ports will be 5000 for http and 7001 for https, respectively.
* The connectionstring for the database is located in ```appsettings.json```
* Adapted folder structure (DTOs, Services,...)
* Simple custom logger
* Some log flags in ```appsettings.json``` are set to ```Warning```
* In ```launchsettings.json``` two settings are configured: one (the default) for http, another one for https.

The following extension methods are available:
* Copy properties to a DTO: ```T CopyFrom<T>(this T target, object source)``` or ```T CopyPropertiesFrom<T>(this T target, object source)```
* Transform object to a record: ```T TransformTo<T>(this object source)```
* Log in a controller: ```void Log(this ControllerBase controller, string msg = "")```
* Log in a Hub: ```Log(this Microsoft.AspNetCore.SignalR.Hub hub, string msg = "")```

## Prerequesites
* .net9 SDK installed. Test with the following command (should show 9.0.0 or higher):
  ```
  dotnet --version
  ```
* tool dotnet-ef version 9 installed. Test with the following command (should show 9.0.0 or higher):
  ```
  dotnet-ef --version
  ```

## Usage

```
  createWebApi solutionName targetFolder [pathToDbFile] [tableName]
  createMinApi solutionName targetFolder [pathToDbFile] [tableName]
  createAllApi solutionName targetFolder [pathToDbFile] [tableName]
```

**Types**

|command   |description |
|:---------|:-----------|
|createWebApi|	Creates backend with controllers
|createMinApi|	Creates backend with minimal API
|createAllApi|	Creates backend with both minimal API and controllers


**Parameters**

|parameter   |optional|description|default |
|:-----------|:------:|:----------|:-------|
|solutionName|	      |Name of the solution. Also used as name of project and folder|
|targetFolder|        |Folder where to generate the project to.|
|pathToDbFile|    *   |If set, an Entity data model will be generated.|
|tableName	 |    *   |For this table a Get-Endpoint is generated.|Categories|

### Examples
1. Create backend using controllers named *MyBackend* into a local subfolder without using a database
   ```
   createWebApi MyBackend .
   ```
1. Create backend using controllers into a local subfolder using Northwind SQLServer database - generated REST service will use table *Categories*
   ```
   createWebApi NorthwindManager . Northwnd.mdf
   ```
1. Create backend with minimal API to *D:\Temp* using Northwind Sqlite database - generated REST service will use table *Products*
   ```
   createMinApi NorthwindManager D:\Temp Northwnd.sqlite Products
   ```
1. Create backend with minimal API and controllers to *C:\Temp* using Northwind SQLServer database located in *D:\Temp* - generated REST service will use table *Employees*
   ```
   createAllApi NorthwindManager D:\Temp C:\Temp\Northwnd.mdf Employees
   ```