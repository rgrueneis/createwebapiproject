﻿namespace $$PROJECT$$.Controllers;

public record struct OkStatus(bool IsOk, int Nr, string? Error = null);

[Route("[controller]")]
[ApiController]
public class ValuesController($$DBNAME$$Context db) : ControllerBase
{

  [HttpGet("$$TABLE$$")]
  public OkStatus Get$$TABLE$$ToTest()
  {
    this.Log();
    try
    {
      int nr = db.$$TABLE$$.Count();
      return new OkStatus(true, nr);
    }
    catch (Exception exc)
    {
      return new OkStatus(false, -1, exc.Message);
    }
  }
}
