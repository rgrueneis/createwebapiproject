@echo off
if "%1"=="" (
  echo [95musage:[0m createWebApi solutionName targetFolder [pathToDbFile] [tableName]
  echo [96msolutionName[0m: Name of the solution and name of folder
  echo [96mtargetFolder[0m: Folder where to generate the project to. Default: Subfolder with solution name in current folder
  echo [96mpathToDbFile[0m: If set, an Entity data model will be generated. DB has to reside in local folder or absolute path given.
  echo [96mtableName[0m:    For this table a Get-WebService is generated. Default: Categories
  exit/b
)
@call .impl.bat Web %1 %2 %3 %4